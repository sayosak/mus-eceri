package com.example.projet;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;


import android.content.Context;
import android.content.Intent;
import android.database.Cursor;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Parcelable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

public class SearchActivity extends AppCompatActivity {
    private static Map<CollectionItem, ImageView> imageViewsItems = new HashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);


        Intent intent = getIntent();
        this.setTitle("Recherche : " + intent.getStringExtra("request"));

        final RecyclerView rv = (RecyclerView) findViewById(R.id.searchList);
        rv.setLayoutManager(new LinearLayoutManager(this));
        rv.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        rv.setAdapter(new MyAdapter(intent.<CollectionItem>getParcelableArrayListExtra("list")));

    }

    class MyAdapter extends RecyclerView.Adapter<RowHolder> {

        ArrayList<CollectionItem> items;


        public MyAdapter(ArrayList<CollectionItem> items) {
            this.items = items;
        }

        @NonNull
        @Override
        public RowHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return (new RowHolder(getLayoutInflater().inflate(R.layout.row, parent, false)));
        }


        @Override
        public void onBindViewHolder(@NonNull RowHolder holder, int position) {
            holder.bindModel(items.get(position));
            //holder.bindModel(colItems[position]);

        }

        @Override
        public int getItemCount() {
            return (items.size());
        }
    }

    class RowHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView name = null;
        TextView category = null;
        TextView brand = null;
        ImageView itemImage = null;
        CollectionItem ci;

        public RowHolder(@NonNull View row) {
            super(row);
            row.setOnClickListener(this);
            name = (TextView) row.findViewById(R.id.name);
            category = (TextView) row.findViewById(R.id.categories);
            brand = (TextView) row.findViewById(R.id.brand);
            itemImage = (ImageView) row.findViewById(R.id.itemImage);
        }

        @Override
        public void onClick(View v) {
            Intent intent;
            if (ci.pictures != null) {
                intent = new Intent(SearchActivity.this, CollectionItemActivity.class);
            } else {
                intent = new Intent(SearchActivity.this, CollectionItemActivityWithoutPictures.class);
            }

            intent.putExtra(CollectionItem.TAG, (Parcelable) ci);
            startActivity(intent);
        }


        void bindModel(CollectionItem item) {
            ci = item;
            name.setText(ci.name);
            category.setText(ci.categories);
            if (ci.brand != null) {
                brand.setText(ci.brand);
            }
            imageViewsItems.put(ci, itemImage);
            new DownloadImageTask().execute(ci);
        }
    }

    private class DownloadImageTask extends AsyncTask<CollectionItem, Void, Pair> {

        @Override
        protected Pair doInBackground(CollectionItem... collectionItems) {

            Bitmap IconBadge = null;
            try {
                URL url = new URL(collectionItems[0].thumbnail);
                HttpURLConnection urlConnection = (HttpsURLConnection) url.openConnection();
                try {
                    InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                    IconBadge = BitmapFactory.decodeStream(in);
                } finally {
                    urlConnection.disconnect();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            return new Pair(collectionItems[0], IconBadge);
        }

        @Override
        protected void onPostExecute(Pair result) {
            super.onPostExecute(result);
            imageViewsItems.get(result.getCi()).setImageBitmap(result.getBitmap());
        }
    }
}
