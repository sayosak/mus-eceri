package com.example.projet;

import android.graphics.Bitmap;

public class Pair {
    private CollectionItem ci;
    private Bitmap bitmap;

    public Pair(CollectionItem ci, Bitmap bitmap) {
        this.ci = ci;
        this.bitmap = bitmap;
    }

    public CollectionItem getCi() {
        return ci;
    }

    public Bitmap getBitmap() {
        return bitmap;
    }
}
