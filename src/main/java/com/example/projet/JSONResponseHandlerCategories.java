package com.example.projet;

import android.util.JsonReader;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

public class JSONResponseHandlerCategories {
    private List<String> categoriesList;

    public JSONResponseHandlerCategories(List<String> categoriesList) {
        this.categoriesList = categoriesList;
    }

    public void readJsonStream(InputStream response) throws IOException {
        JsonReader reader = new JsonReader(new InputStreamReader(response, "UTF-8"));
        try {
            readCategoriesArray(reader);
        } finally {
            reader.close();
        }
    }

    public void readCategoriesArray(JsonReader reader) throws IOException {
        reader.beginArray();
        while (reader.hasNext()) {
            String categorie = reader.nextString();
            categoriesList.add(categorie);
        }
        reader.endArray();
    }
}
