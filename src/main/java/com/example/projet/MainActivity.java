package com.example.projet;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

import static android.database.sqlite.SQLiteDatabase.CONFLICT_IGNORE;

public class MainActivity extends AppCompatActivity {
    private static List<CollectionItem > collectionItems = new ArrayList<>();
    DbHelper collectionDbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        final ImageView ceriIcon = findViewById(R.id.ceriIcon);
        final Button collectionBtn = findViewById(R.id.collectionBtn);
        final Button collectionBtnYear = findViewById(R.id.collectionBtnYear);
        final Button collectionCatBtn = findViewById(R.id.collectionCatBtn);

        new UpdateTask().execute(collectionItems);


        ceriIcon.setImageResource(R.drawable.ceri);
        collectionBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, CollectionActivity.class);
                intent.putExtra("orderType","alphabet");
                startActivity(intent);
            }
        });

        collectionBtnYear.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, CollectionActivity.class);
                intent.putExtra("orderType","chrono");
                startActivity(intent);
            }
        });

        collectionCatBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, CollectionCategoriesActivity.class);
                startActivity(intent);
            }
        });


    }

    public class UpdateTask extends AsyncTask<List<CollectionItem>, String, List<CollectionItem>> {


        @Override
        protected List<CollectionItem> doInBackground(List<CollectionItem>... lists) {

            try {
                // Team URL
                URL url = WebServiceUrl.buildSearchCatalog();
                HttpURLConnection urlConnection = (HttpsURLConnection) url.openConnection();
                JSONResponseHandlerCatalog jsrCatalog = new JSONResponseHandlerCatalog(lists[0]);
                try {
                    InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                    jsrCatalog.readJsonStream(in);
                } finally {
                    urlConnection.disconnect();
                }

            } catch (IOException e) {
                e.printStackTrace();
            }

            return lists[0];
        }


        @Override
        protected void onPostExecute(List<CollectionItem> collectionItems) {
            super.onPostExecute(collectionItems);
            updateList(collectionItems);
            collectionDbHelper = new DbHelper(MainActivity.this,collectionItems);
            collectionDbHelper.populate();
            collectionDbHelper.addCategoriesTables();
        }

    }

    private void updateList(List<CollectionItem> colItems) {
        collectionItems = colItems;
    }
}
