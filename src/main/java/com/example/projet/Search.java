package com.example.projet;

import android.content.Context;
import android.content.Intent;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;

public class Search {

    public static void getSearchItems(EditText searchText, Context activity) {
        ArrayList<CollectionItem> searchListItems = new ArrayList<>();
        String request = searchText.getText().toString();
        if (!request.equals("")) {
            String[] words = request.split(" ");
            for (int i = 0; i < words.length; i++) {
                String word = words[i];
                if (i == 0) {
                    for (CollectionItem ci : DbHelper.collectionItems) {
                        if (ci.checkSearch(word)) {
                            searchListItems.add(ci);
                        }
                    }
                } else {
                    ArrayList<CollectionItem> searchListItemsActualWord = new ArrayList<>();
                    word = manageString(word);
                    for (CollectionItem ci : DbHelper.collectionItems) {
                        if (ci.checkSearch(word)) {
                            searchListItemsActualWord.add(ci);
                        }
                    }
                    searchListItems = updateSearchItems(searchListItems, searchListItemsActualWord);
                }
            }
            if (searchListItems.size() > 0) {
                Intent intent = new Intent(activity, SearchActivity.class);
                intent.putParcelableArrayListExtra("list", searchListItems);
                intent.putExtra("request", request);
                activity.startActivity(intent);
            } else {
                mt("Aucun élement n'a été trouvé", activity);
            }
        } else {
            mt("Champ recharche vide", activity);
        }
    }

    private static String manageString(String word) {
        word = word.toLowerCase().trim();
        return word;
    }


    private static ArrayList<CollectionItem> updateSearchItems(ArrayList<CollectionItem> searchItems, ArrayList<CollectionItem> searchItemsActualWord) {
        ArrayList newSearachItemsList = new ArrayList(searchItems);
        for (CollectionItem ci : searchItems) {
            if (!searchItemsActualWord.contains(ci)) {
                newSearachItemsList.remove(ci);
            }
        }
        return newSearachItemsList;
    }

    public static void mt(String string, Context activity) {
        Toast.makeText(activity, string, Toast.LENGTH_SHORT).show();
    }
}
