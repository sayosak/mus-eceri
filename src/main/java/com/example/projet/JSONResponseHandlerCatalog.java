package com.example.projet;

import android.util.JsonReader;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

public class JSONResponseHandlerCatalog {
    private List<CollectionItem> collectionItems;

    public JSONResponseHandlerCatalog(List<CollectionItem> collectionItems) {
        this.collectionItems = collectionItems;
    }

    public void readJsonStream(InputStream response) throws IOException {
        JsonReader reader = new JsonReader(new InputStreamReader(response, "UTF-8"));
        try {
            readCatalog(reader);
        } finally {
            reader.close();
        }
    }

    public void readCatalog(JsonReader reader) throws IOException {
        reader.beginObject();
        while (reader.hasNext()) {
            CollectionItem ci = new CollectionItem();
            String name = reader.nextName();
            readCatalogArray(reader, ci);
            updateItem(ci, name);
            collectionItems.add(ci);
        }
        reader.endObject();
    }


    private void readCatalogArray(JsonReader reader, CollectionItem ci) throws IOException {

        reader.beginObject();
        int nb = 0; // only consider the first element of the array
        String allStrings = "";
        int firstTimeFrame = 0;
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (name.equals("name")) {
                ci.name = reader.nextString();
                allStrings = allStrings + manageString(ci.name) + " ";
            } else if (name.equals("brand")) {
                ci.brand = reader.nextString();
                allStrings = allStrings + manageString(ci.brand) + " ";
            } else if (name.equals("categories")) {
                reader.beginArray();
                String categories = "";
                while (reader.hasNext()) {
                    String categoriesToAdd = reader.nextString();
                    categories = categories + " - " + categoriesToAdd + "\n";
                    allStrings = allStrings + manageString(categoriesToAdd) + " ";
                    ci.categoriesList.add(categoriesToAdd);
                }
                reader.endArray();
                ci.categories = categories;
            } else if (name.equals("technicalDetails")) {
                reader.beginArray();
                String technicalDetails = "";
                while (reader.hasNext()) {
                    String technicalDetailToAdd = reader.nextString();
                    technicalDetails = technicalDetails + " - " + technicalDetailToAdd + "\n";
                    allStrings = allStrings + manageString(technicalDetailToAdd) + " ";
                }
                reader.endArray();
                ci.technicalDetails = technicalDetails;
            } else if (name.equals("year")) {
                ci.year = (int) reader.nextDouble();
                allStrings = allStrings + manageString(String.valueOf(ci.year)) + " ";
            } else if (name.equals("description")) {
                ci.description = reader.nextString();
                allStrings = allStrings + manageString(ci.description) + " ";
            } else if (name.equals("timeFrame")) {
                reader.beginArray();
                String timeFrame = "";
                while (reader.hasNext()) {
                    int timeFrameToAdd = reader.nextInt();
                    timeFrame = timeFrame + String.valueOf(timeFrameToAdd) + "\n";
                    allStrings = allStrings + manageString(String.valueOf(timeFrameToAdd)) + " ";
                    if (firstTimeFrame == 0) {
                        firstTimeFrame = timeFrameToAdd;
                    }
                }
                reader.endArray();
                ci.timeFrame = timeFrame;
            } else if (name.equals("working")) {
                if (reader.nextBoolean()) {
                    ci.working = "Functional";
                } else {
                    ci.working = "Not Functional";
                }
            } else if (name.equals("pictures")) {
                reader.beginObject();
                String reusltPictures = "";
                while (reader.hasNext()) {
                    reusltPictures = reusltPictures + reader.nextName() + ",";
                    reader.skipValue();
                }
                ci.pictures = reusltPictures;
                reader.endObject();
            } else {
                reader.skipValue();
            }

        }
        setYear(ci, firstTimeFrame);
        ci.search = allStrings;
        reader.endObject();
    }

    private void setYear(CollectionItem ci, int firstTimeFrame) {
        if (ci.year == 0) {
            ci.year = firstTimeFrame;
        }
    }

    private String manageString(String s) {
        s = s.replaceAll(",", "");
        s = s.replaceAll("-", " ");
        s = s.replaceAll("\\p{P}", "");
        s = s.toLowerCase().trim();
        return s;
    }

    private void updateItem(CollectionItem ci, String id) {
        ci.id = id;
        ci.thumbnail = "https://demo-lia.univ-avignon.fr/cerimuseum/items/" + id + "/thumbnail";
    }
}


