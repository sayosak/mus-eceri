package com.example.projet;

import android.net.Uri;

import java.net.MalformedURLException;
import java.net.URL;

public class WebServiceUrl {

    // demo-lia.univ-avignon.fr/cerimuseum/
    // constant string used as s parameter for lookuptable
    private static final String CUR_YEAR = "1920";

    private static final String HOST = "demo-lia.univ-avignon.fr";
    private static final String MUSEUM = "cerimuseum";

    private static Uri.Builder commonBuilder() {
        Uri.Builder builder = new Uri.Builder();

        builder.scheme("https")
                .authority(HOST)
                .appendPath(MUSEUM);
        return builder;
    }

    // Get information on the entire collection
    // https://demo-lia.univ-avignon.fr/cerimuseum/catalog
    private static final String SEARCH_CATALOG = "catalog";

    // Build URL to get information for a specific team
    public static URL buildSearchCatalog() throws MalformedURLException {
        Uri.Builder builder = commonBuilder();
        builder.appendPath(SEARCH_CATALOG);
        URL url = new URL(builder.build().toString());
        return url;
    }

    // Get all categories
    // https://demo-lia.univ-avignon.fr/cerimuseum/categories
    private static final String SEARCH_CATEGORIES = "categories";

    // Build URL to get information for a specific team
    public static URL buildSearchCategories() throws MalformedURLException {
        Uri.Builder builder = commonBuilder();
        builder.appendPath(SEARCH_CATEGORIES);
        URL url = new URL(builder.build().toString());
        return url;
    }

}
