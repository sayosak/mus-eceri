package com.example.projet;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Parcelable;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.CursorAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

public class CollectionActivity extends AppCompatActivity {
    ;
    private static Map<CollectionItem, ImageView> imageViewsItems = new HashMap<>();
    private DbHelper collectionDbHelper;
    private String orderType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_collection);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        collectionDbHelper = new DbHelper(CollectionActivity.this);
        orderType = getIntent().getStringExtra("orderType");


        final ImageButton searchBtn = findViewById(R.id.serchBtn);
        final EditText searchText = findViewById(R.id.searchText);
        final RecyclerView rv = (RecyclerView) findViewById(R.id.collection);
        rv.setLayoutManager(new LinearLayoutManager(this));
        rv.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        rv.setAdapter(new MyAdapter(CollectionActivity.this));

        List<CollectionItem> l = DbHelper.collectionItems;

        searchText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    Search.getSearchItems(searchText, CollectionActivity.this);
                    handled = true;
                }
                return handled;
            }
        });
        searchBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Search.getSearchItems(searchText, CollectionActivity.this);
            }
        });
    }

    class MyAdapter extends RecyclerView.Adapter<RowHolder> {

        CursorAdapter cursorAdapter;

        Context mContext;

        public MyAdapter(Context context) {
            mContext = context;
            if (orderType.equals("alphabet")) {
                cursorAdapter = new CursorAdapter(mContext, collectionDbHelper.fetchAllItemsAlphabet(), 0) {
                    @Override
                    public View newView(Context context, Cursor cursor, ViewGroup parent) {
                        return null;
                    }

                    @Override
                    public void bindView(View view, Context context, Cursor cursor) {

                    }
                };
            } else if (orderType.equals("chrono")) {
                cursorAdapter = new CursorAdapter(mContext, collectionDbHelper.fetchAllItemsYear(), 0) {
                    @Override
                    public View newView(Context context, Cursor cursor, ViewGroup parent) {
                        return null;
                    }

                    @Override
                    public void bindView(View view, Context context, Cursor cursor) {

                    }
                };
            }

        }

        @NonNull
        @Override
        public RowHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

            return (new RowHolder(getLayoutInflater().inflate(R.layout.row, parent, false)));
        }


        @Override
        public void onBindViewHolder(@NonNull RowHolder holder, int position) {
            cursorAdapter.getCursor().moveToPosition(position);
            holder.bindModel(cursorAdapter.getCursor());
        }

        @Override
        public int getItemCount() {
            return (cursorAdapter.getCount());
        }
    }

    class RowHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView name = null;
        TextView category = null;
        TextView brand = null;
        ImageView itemImage = null;
        CollectionItem ci;

        public RowHolder(@NonNull View row) {
            super(row);
            row.setOnClickListener(this);
            name = (TextView) row.findViewById(R.id.name);
            category = (TextView) row.findViewById(R.id.categories);
            brand = (TextView) row.findViewById(R.id.brand);
            itemImage = (ImageView) row.findViewById(R.id.itemImage);
        }

        @Override
        public void onClick(View v) {
            Intent intent;
            if (ci.pictures != null) {
                intent = new Intent(CollectionActivity.this, CollectionItemActivity.class);
            } else {
                intent = new Intent(CollectionActivity.this, CollectionItemActivityWithoutPictures.class);
            }

            intent.putExtra(CollectionItem.TAG, (Parcelable) ci);
            startActivity(intent);
        }


        void bindModel(Cursor item) {
            ci = collectionDbHelper.cursorToItem(item);
            name.setText(ci.name);
            category.setText(ci.categories);
            if (ci.brand != null) {
                brand.setText(ci.brand);
            }
            imageViewsItems.put(ci, itemImage);
            new DownloadImageTask().execute(ci);
        }
    }

    private class DownloadImageTask extends AsyncTask<CollectionItem, Void, Pair> {

        @Override
        protected Pair doInBackground(CollectionItem... collectionItems) {

            Bitmap IconBadge = null;
            try {
                URL url = new URL(collectionItems[0].thumbnail);
                HttpURLConnection urlConnection = (HttpsURLConnection) url.openConnection();
                try {
                    InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                    IconBadge = BitmapFactory.decodeStream(in);
                } finally {
                    urlConnection.disconnect();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            return new Pair(collectionItems[0], IconBadge);
        }

        @Override
        protected void onPostExecute(Pair result) {
            super.onPostExecute(result);
            imageViewsItems.get(result.getCi()).setImageBitmap(result.getBitmap());
        }
    }
}
