package com.example.projet;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

public class CollectionItem implements Parcelable {
    public static final String TAG = CollectionItem.class.getSimpleName();

    String id;
    String name;
    String thumbnail;
    String categories;
    List<String> categoriesList = new ArrayList<>();
    String description;
    String timeFrame;

    int year;
    String brand;
    String technicalDetails;
    String working;
    String pictures;
    String search;

    public CollectionItem(String id, String name, String categories, String description, String timeFrame, double year, String brand, String technicalDetails, String working, String pictures, String search) {
        this.id = id;
        this.name = name;
        this.thumbnail = "https://demo-lia.univ-avignon.fr/cerimuseum/items/" + id + "/thumbnail";
        this.categories = categories;
        this.description = description;
        this.timeFrame = timeFrame;
        this.year = (int) year;
        this.brand = brand;
        this.technicalDetails = technicalDetails;
        this.working = working;
        this.pictures = pictures;
        this.search = search;
    }


    public CollectionItem() {
    }

    public boolean checkSearch(String wordToFind) {
        String[] words = search.split(" ");
        for (String word : words) {
            if (word.equals(wordToFind)) {
                return true;
            }
        }
        return false;
    }

    protected CollectionItem(Parcel in) {
        id = in.readString();
        name = in.readString();
        thumbnail = in.readString();
        categories = in.readString();
        categoriesList = in.createStringArrayList();
        description = in.readString();
        timeFrame = in.readString();
        year = in.readInt();
        brand = in.readString();
        technicalDetails = in.readString();
        working = in.readString();
        pictures = in.readString();
        search = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(thumbnail);
        dest.writeString(categories);
        dest.writeStringList(categoriesList);
        dest.writeString(description);
        dest.writeString(timeFrame);
        dest.writeInt(year);
        dest.writeString(brand);
        dest.writeString(technicalDetails);
        dest.writeString(working);
        dest.writeString(pictures);
        dest.writeString(search);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<CollectionItem> CREATOR = new Creator<CollectionItem>() {
        @Override
        public CollectionItem createFromParcel(Parcel in) {
            return new CollectionItem(in);
        }

        @Override
        public CollectionItem[] newArray(int size) {
            return new CollectionItem[size];
        }
    };
}
