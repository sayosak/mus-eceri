package com.example.projet;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Parcelable;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.CursorAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

public class CollectionCategoriesActivity extends AppCompatActivity {
    private static Map<CollectionItem, ImageView> imageViewsItems = new HashMap<>();
    private DbHelper collectionDbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_collection_categories);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        collectionDbHelper = new DbHelper(CollectionCategoriesActivity.this);

        final ImageButton searchBtn = findViewById(R.id.serchBtn);
        final EditText searchText = findViewById(R.id.searchText);
        ListView categoriesListView = findViewById(R.id.categoriesListView);
        String[] values = new String[DbHelper.categoriesList.size()];
        categoriesListView.setAdapter(new MySimpleArrayAdapter(CollectionCategoriesActivity.this, DbHelper.categoriesList.toArray(values)));

        searchText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    Search.getSearchItems(searchText, CollectionCategoriesActivity.this);
                    handled = true;
                }
                return handled;
            }
        });
        searchBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Search.getSearchItems(searchText, CollectionCategoriesActivity.this);
            }
        });
    }


    public class MySimpleArrayAdapter extends ArrayAdapter<String> {
        private final Context context;
        private final String[] values;

        public MySimpleArrayAdapter(Context context, String[] values) {
            super(context, -1, values);
            this.context = context;
            this.values = values;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View rowView = inflater.inflate(R.layout.row_categories, parent, false);
            RecyclerView itemsList = (RecyclerView) rowView.findViewById(R.id.itemsCategorieList);
            TextView categorieTitle = (TextView) rowView.findViewById(R.id.categorieTitle);
            categorieTitle.setText(values[position]);


            itemsList.setNestedScrollingEnabled(false);
            itemsList.setLayoutManager(new LinearLayoutManager(CollectionCategoriesActivity.this));
            itemsList.addItemDecoration(new DividerItemDecoration(CollectionCategoriesActivity.this, LinearLayoutManager.VERTICAL));
            itemsList.setAdapter(new MyAdapter(values[position], CollectionCategoriesActivity.this));


            return rowView;
        }
    }


    class MyAdapter extends RecyclerView.Adapter<RowHolder> {

        CursorAdapter cursorAdapter;

        Context mContext;

        public MyAdapter(String category, Context context) {
            mContext = context;
            cursorAdapter = new CursorAdapter(mContext, collectionDbHelper.fetchCategoryItems(category), 0) {
                @Override
                public View newView(Context context, Cursor cursor, ViewGroup parent) {
                    return null;
                }

                @Override
                public void bindView(View view, Context context, Cursor cursor) {

                }
            };
        }

        @NonNull
        @Override
        public RowHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

            return (new RowHolder(getLayoutInflater().inflate(R.layout.row, parent, false)));
        }


        @Override
        public void onBindViewHolder(@NonNull RowHolder holder, int position) {
            cursorAdapter.getCursor().moveToPosition(position);
            holder.bindModel(cursorAdapter.getCursor());
        }

        @Override
        public int getItemCount() {
            return (cursorAdapter.getCount());
        }
    }

    class RowHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView name = null;
        TextView category = null;
        TextView brand = null;
        ImageView itemImage = null;
        CollectionItem ci;

        public RowHolder(@NonNull View row) {
            super(row);
            row.setOnClickListener(this);
            name = (TextView) row.findViewById(R.id.name);
            category = (TextView) row.findViewById(R.id.categories);
            brand = (TextView) row.findViewById(R.id.brand);
            itemImage = (ImageView) row.findViewById(R.id.itemImage);
        }

        @Override
        public void onClick(View v) {
            Intent intent;
            if (ci.pictures != null) {
                intent = new Intent(CollectionCategoriesActivity.this, CollectionItemActivity.class);
            } else {
                intent = new Intent(CollectionCategoriesActivity.this, CollectionItemActivityWithoutPictures.class);
            }

            intent.putExtra(CollectionItem.TAG, (Parcelable) ci);
            startActivity(intent);
        }


        void bindModel(Cursor item) {
            ci = collectionDbHelper.cursorToItem(item);
            name.setText(ci.name);
            category.setText(ci.categories);
            if (ci.brand != null) {
                brand.setText(ci.brand);
            }
            imageViewsItems.put(ci, itemImage);
            new DownloadImageTask().execute(ci);
        }
    }

    private class DownloadImageTask extends AsyncTask<CollectionItem, Void, Pair> {

        @Override
        protected Pair doInBackground(CollectionItem... collectionItems) {

            Bitmap IconBadge = null;
            try {
                URL url = new URL(collectionItems[0].thumbnail);
                HttpURLConnection urlConnection = (HttpsURLConnection) url.openConnection();
                try {
                    InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                    IconBadge = BitmapFactory.decodeStream(in);
                } finally {
                    urlConnection.disconnect();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            return new Pair(collectionItems[0], IconBadge);
        }

        @Override
        protected void onPostExecute(Pair result) {
            super.onPostExecute(result);
            imageViewsItems.get(result.getCi()).setImageBitmap(result.getBitmap());
        }
    }

}
