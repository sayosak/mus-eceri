package com.example.projet;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

import static android.database.sqlite.SQLiteDatabase.CONFLICT_IGNORE;

public class DbHelper extends SQLiteOpenHelper {
    public static List<CollectionItem> collectionItems = new ArrayList<>();

    private static final String TAG = DbHelper.class.getSimpleName();

    // If you change the database schema, you must increment the database version.
    private static final int DATABASE_VERSION = 1;

    public static final String DATABASE_NAME = "collection.db";

    public static final String TABLE_NAME = "collection";

    public static final String _ID = "_id";
    public static final String COLUMN_ITEM_ID = "id";
    public static final String COLUMN_ITEM_NAME = "name";
    public static final String COLUMN_ITEM_BRAND = "brand";
    public static final String COLUMN_ITEM_THUMBNAIL = "thumbnail";
    public static final String COLUMN_ITEM_CATEGORIES = "categories";
    public static final String COLUMN_ITEM_DESCRIPTION = "description";
    public static final String COLUMN_ITEM_YEAR = "year";
    public static final String COLUMN_ITEM_TECHNICALDETAILS = "technicalDetails";
    public static final String COLUMN_ITEM_TIMEFRAME = "timeFrame";
    public static final String COLUMN_ITEM_WORKING = "working";
    public static final String COLUMN_ITEM_PICTURES = "pictures";
    public static final String COLUMN_ITEM_SEARCH = "search";


    public static boolean ifPopulate = true;
    public static List<String> categoriesList = new ArrayList<>();
    public static List<String> categoriesListSQL = new ArrayList<>();
    public static Map<String, List<CollectionItem>> categoriesItemsMap = new HashMap<>();

    public DbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        ifPopulate = false;
    }

    public DbHelper(Context context, List<CollectionItem> colItems) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        ifPopulate = false;
        collectionItems = colItems;
        new UpdateCategoriesTask().execute(categoriesList);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME + ";");

        final String SQL_CREATE_BOOK_TABLE = "CREATE TABLE " + TABLE_NAME + " (" +
                _ID + " INTEGER PRIMARY KEY," +
                COLUMN_ITEM_ID + " TEXT NOT NULL," +
                COLUMN_ITEM_NAME + " TEXT NOT NULL," +
                COLUMN_ITEM_THUMBNAIL + " TEXT," +
                COLUMN_ITEM_BRAND + " TEXT," +
                COLUMN_ITEM_CATEGORIES + " TEXT," +
                COLUMN_ITEM_DESCRIPTION + " TEXT," +
                COLUMN_ITEM_YEAR + " DOUBLE," +
                COLUMN_ITEM_TECHNICALDETAILS + " TEXT," +
                COLUMN_ITEM_TIMEFRAME + " TEXT," +
                COLUMN_ITEM_WORKING + " TEXT," +
                COLUMN_ITEM_PICTURES + " TEXT," +
                COLUMN_ITEM_SEARCH + " TEXT," +
                " UNIQUE (" + COLUMN_ITEM_ID + ", " +
                COLUMN_ITEM_NAME + ") ON CONFLICT ROLLBACK);";

        db.execSQL(SQL_CREATE_BOOK_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table if exists " + TABLE_NAME); // drops the old database
        onCreate(db); // run onCreate to get new database
    }

    public void addCategoriesTables() {
        setItemsCategoriesMap();
        formatCategroiesNames();
        SQLiteDatabase db = this.getWritableDatabase();
        for (String categoryTableName : categoriesListSQL) {
            db.execSQL("DROP TABLE IF EXISTS " + categoryTableName + ";");

            final String SQL_CREATE_CATEGORY_TABLE = "CREATE TABLE " + categoryTableName + " (" +
                    _ID + " INTEGER PRIMARY KEY," +
                    COLUMN_ITEM_ID + " TEXT NOT NULL," +
                    COLUMN_ITEM_NAME + " TEXT NOT NULL," +
                    COLUMN_ITEM_THUMBNAIL + " TEXT," +
                    COLUMN_ITEM_BRAND + " TEXT," +
                    COLUMN_ITEM_CATEGORIES + " TEXT," +
                    COLUMN_ITEM_DESCRIPTION + " TEXT," +
                    COLUMN_ITEM_YEAR + " DOUBLE," +
                    COLUMN_ITEM_TECHNICALDETAILS + " TEXT," +
                    COLUMN_ITEM_TIMEFRAME + " TEXT," +
                    COLUMN_ITEM_WORKING + " TEXT," +
                    COLUMN_ITEM_PICTURES + " TEXT," +
                    COLUMN_ITEM_SEARCH + " TEXT," +
                    " UNIQUE (" + COLUMN_ITEM_ID + ", " +
                    COLUMN_ITEM_NAME + ") ON CONFLICT ROLLBACK);";

            db.execSQL(SQL_CREATE_CATEGORY_TABLE);
        }
        addItemsByCategory();
    }

    public void addItems() {
        for (CollectionItem ct : collectionItems) {
            SQLiteDatabase db = this.getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put(COLUMN_ITEM_NAME, ct.name);
            values.put(COLUMN_ITEM_ID, ct.id);
            values.put(COLUMN_ITEM_THUMBNAIL, ct.thumbnail);
            values.put(COLUMN_ITEM_BRAND, ct.brand);
            values.put(COLUMN_ITEM_CATEGORIES, ct.categories);
            values.put(COLUMN_ITEM_DESCRIPTION, ct.description);
            values.put(COLUMN_ITEM_YEAR, ct.year);
            values.put(COLUMN_ITEM_TECHNICALDETAILS, ct.technicalDetails);
            values.put(COLUMN_ITEM_TIMEFRAME, ct.timeFrame);
            values.put(COLUMN_ITEM_WORKING, ct.working);
            values.put(COLUMN_ITEM_PICTURES, ct.pictures);
            values.put(COLUMN_ITEM_SEARCH, ct.search);
            Log.d(TAG, "adding: " + ct.name + " with id=" + ct.id);

            // Inserting Row
            // The unique used for creating table ensures to have only one copy of each pair (team, championship)
            // If rowID = -1, an error occured
            long rowID = db.insertWithOnConflict(TABLE_NAME, null, values, CONFLICT_IGNORE);
            db.close(); // Closing database connection
        }
    }

    public void addItemsByCategory() {
        for (String category : categoriesList) {
            String tableName = stripAccents(category);
            List<CollectionItem> items = categoriesItemsMap.get(category);
            for (CollectionItem ct : items) {
                SQLiteDatabase db = this.getWritableDatabase();

                ContentValues values = new ContentValues();
                values.put(COLUMN_ITEM_NAME, ct.name);
                values.put(COLUMN_ITEM_ID, ct.id);
                values.put(COLUMN_ITEM_THUMBNAIL, ct.thumbnail);
                values.put(COLUMN_ITEM_BRAND, ct.brand);
                values.put(COLUMN_ITEM_CATEGORIES, ct.categories);
                values.put(COLUMN_ITEM_DESCRIPTION, ct.description);
                values.put(COLUMN_ITEM_YEAR, ct.year);
                values.put(COLUMN_ITEM_TECHNICALDETAILS, ct.technicalDetails);
                values.put(COLUMN_ITEM_TIMEFRAME, ct.timeFrame);
                values.put(COLUMN_ITEM_WORKING, ct.working);
                values.put(COLUMN_ITEM_PICTURES, ct.pictures);
                values.put(COLUMN_ITEM_SEARCH, ct.search);
                Log.d(TAG, "adding: " + ct.name + " in " + category);

                // Inserting Row
                // The unique used for creating table ensures to have only one copy of each pair (team, championship)
                // If rowID = -1, an error occured
                long rowID = db.insertWithOnConflict(tableName, null, values, CONFLICT_IGNORE);
                db.close(); // Closing database connection
            }
        }
    }

    public void populate() {
        Log.d(TAG, "call populate()");
        addItems();
        SQLiteDatabase db = this.getReadableDatabase();
        long numRows = DatabaseUtils.longForQuery(db, "SELECT COUNT(*) FROM " + TABLE_NAME, null);
        Log.d(TAG, "nb of rows=" + numRows);
        db.close();
    }

    public Cursor fetchAllItemsAlphabet() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_NAME, null,
                null, null, null, null, COLUMN_ITEM_NAME + " ASC", null);

        Log.d(TAG, "call fetchAllItemsAlphabet()");
        if (cursor != null) {
            cursor.moveToFirst();
        }
        return cursor;
    }

    public Cursor fetchAllItemsYear() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_NAME, null,
                null, null, null, null, COLUMN_ITEM_YEAR, null);

        Log.d(TAG, "call fetchAllItemsAlphabet()");
        if (cursor != null) {
            cursor.moveToFirst();
        }
        return cursor;
    }

    public Cursor fetchCategoryItems(String tableName) {
        tableName = stripAccents(tableName);
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(tableName, null,
                null, null, null, null, COLUMN_ITEM_NAME + " ASC", null);

        Log.d(TAG, "call fetchCategoryItems()");
        if (cursor != null) {
            cursor.moveToFirst();
        }
        return cursor;
    }

    public CollectionItem cursorToItem(Cursor cursor) {


        CollectionItem ci = new CollectionItem(cursor.getString(cursor.getColumnIndex(COLUMN_ITEM_ID)),
                cursor.getString(cursor.getColumnIndex(COLUMN_ITEM_NAME)),
                cursor.getString(cursor.getColumnIndex(COLUMN_ITEM_CATEGORIES)),
                cursor.getString(cursor.getColumnIndex(COLUMN_ITEM_DESCRIPTION)),
                cursor.getString(cursor.getColumnIndex(COLUMN_ITEM_TIMEFRAME)),
                cursor.getDouble(cursor.getColumnIndex(COLUMN_ITEM_YEAR)),
                cursor.getString(cursor.getColumnIndex(COLUMN_ITEM_BRAND)),
                cursor.getString(cursor.getColumnIndex(COLUMN_ITEM_TECHNICALDETAILS)),
                cursor.getString(cursor.getColumnIndex(COLUMN_ITEM_WORKING)),
                cursor.getString(cursor.getColumnIndex(COLUMN_ITEM_PICTURES)),
                cursor.getString(cursor.getColumnIndex(COLUMN_ITEM_SEARCH)));

        return ci;
    }


    public void formatCategroiesNames() {
        for (String name : categoriesList) {
            categoriesListSQL.add(stripAccents(name));

        }
    }

    public String stripAccents(String s) {
        s = Normalizer.normalize(s, Normalizer.Form.NFD);
        s = s.replaceAll("[\\p{InCombiningDiacriticalMarks}]", "");
        s = s.replace("/", "_");
        s = s.replace(" ", "_");
        s = s.replace("8", "eight");
        return s;
    }

    public void setItemsCategoriesMap() {
        for (String category : categoriesList) {
            categoriesItemsMap.put(category, new ArrayList<CollectionItem>());
            for (CollectionItem ci : collectionItems) {
                for (String categoryItem : ci.categoriesList) {
                    if (categoryItem.equals(category)) {
                        categoriesItemsMap.get(category).add(ci);
                    }
                }
            }
        }
    }


    public class UpdateCategoriesTask extends AsyncTask<List<String>, String, List<String>> {


        @Override
        protected List<String> doInBackground(List<String>... lists) {

            try {
                // Team URL
                URL url = WebServiceUrl.buildSearchCategories();
                HttpURLConnection urlConnection = (HttpsURLConnection) url.openConnection();
                JSONResponseHandlerCategories jsrCatalog = new JSONResponseHandlerCategories(lists[0]);
                try {
                    InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                    jsrCatalog.readJsonStream(in);
                } finally {
                    urlConnection.disconnect();
                }

            } catch (IOException e) {
                e.printStackTrace();
            }

            return lists[0];
        }


        @Override
        protected void onPostExecute(List<String> categroies) {
            super.onPostExecute(categroies);
            categoriesList = categroies;
        }
    }

}
