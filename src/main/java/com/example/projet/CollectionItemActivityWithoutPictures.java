package com.example.projet;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.lang.reflect.Array;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

public class CollectionItemActivityWithoutPictures extends AppCompatActivity {

    CollectionItem ci;
    ImageView itemImage;
    TextView name;
    TextView categories;
    TextView description;
    TextView timeFrame;
    TextView year;
    TextView brand;
    TextView technicalDetails;
    TextView working;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_collection_item_without_pictures);

        ci = getIntent().getParcelableExtra(CollectionItem.TAG);
        this.setTitle(ci.name);
        name = findViewById(R.id.name);
        categories = findViewById(R.id.categories);
        description = findViewById(R.id.description);
        timeFrame = findViewById(R.id.timeFrame);
        year = findViewById(R.id.year);
        brand = findViewById(R.id.brand);
        technicalDetails = findViewById(R.id.technicalDetails);
        working = findViewById(R.id.working);
        itemImage = findViewById(R.id.itemImage);

        updateView();

    }

    private void updateView() {
        name.setText(ci.name);
        categories.setText(ci.categories);
        updateDescription();
        timeFrame.setText(ci.timeFrame);
        updateYear();
        updateBrand();
        updateTechnicalDetails();
        updateWorking();
        new DownloadImageTask().execute(ci);
    }

    private void updateWorking() {
        if (ci.working != null) {
            working.setText("Fonctionnel");
        } else {
            working.setText("Non fonctionnel");
        }
    }

    private void updateBrand() {
        if (ci.brand != null) {
            brand.setText(ci.brand);
        } else {
            brand.setText("Non définie");
        }
    }

    private void updateTechnicalDetails() {
        if (ci.technicalDetails != null) {
            technicalDetails.setText(ci.technicalDetails);
        } else {
            technicalDetails.setText("Non définie");
        }
    }

    private void updateYear() {
        if (ci.year != 0) {
            year.setText(String.valueOf(ci.year));
        } else {
            year.setText("Non définie");
        }
    }

    private void updateDescription() {
        if (ci.description != null) {
            description.setText(ci.description);
        } else {
            description.setText("Non définie");
        }
    }


    private class DownloadImageTask extends AsyncTask<CollectionItem, Void, Bitmap> {

        @Override
        protected Bitmap doInBackground(CollectionItem... collectionItems) {
            Bitmap IconBadge = null;
            try {
                URL url = new URL(collectionItems[0].thumbnail);
                HttpURLConnection urlConnection = (HttpsURLConnection) url.openConnection();
                try {
                    InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                    IconBadge = BitmapFactory.decodeStream(in);
                } finally {
                    urlConnection.disconnect();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return IconBadge;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);
            itemImage.setImageBitmap(bitmap);
        }
    }
}
